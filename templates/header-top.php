<div class="top-header">

    <div class="row">

        <div class="col-xs-12 col-md-5 user-signup-header">
            <?php do_action( 'hjiUserGetSignUpMessage' ); ?>
            <?php do_action( 'hjiUserGetSignUpLink' ); ?>
            <?php do_action( 'hjiUserGetWelcomeMessage' ); ?>
            <?php do_action( 'hjiUserGetFavoritesLink' ); ?>
            <?php do_action( 'hjiUserGetSavedSearchesLink' ); ?>
            <?php do_action( 'hjiUserGetLogoutLink' ); ?>
        </div>

        <div class="col-md-7 hidden-xs">
            <?php
                if ( has_nav_menu( 'footer-menu' ) ) :
                    wp_nav_menu( array( 'theme_location' => 'footer-menu', 'menu_class' => 'right' ) );
                endif;
            ?>
        </div>

    </div>
<!-- 

    <div class="row">

        <div class="col-md-12">

            <?php if ( is_active_sidebar( 'blvd-topbar-sidebarwidgets' ) ) : ?>

                <div class="footer-widgets row">

                    <?php dynamic_sidebar( 'blvd-topbar-sidebarwidgets' ); ?>

                </div>

            <?php endif; ?>

         </div>

    </div> -->

</div>